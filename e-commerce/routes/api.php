<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'products'], function(){
    Route::get('/','ProductController@getData');//->middleware('jwt.auth');
    Route::post('/add','ProductController@addData');//->middleware('jwt.auth');
    Route::post('/delete','ProductController@deleteData');//->middleware('jwt.auth');

    //add, get dan delete butuh middleware(harus pakai token)
});

Route::group(['prefix' => 'brands'], function(){
    Route::get('/','BrandController@getAllBrand');//->middleware('jwt.auth');
    Route::post('/add','BrandController@addBrand');//->middleware('jwt.auth');
    Route::post('/delete','BrandController@deleteBrand');//->middleware('jwt.auth');

    //add, get dan delete butuh middleware(harus pakai token)
});

Route::group(['prefix' => 'categories'], function(){
    Route::get('/','CategoryController@getAllCategory');//->middleware('jwt.auth');
    Route::post('/add','CategoryController@addCategory');//->middleware('jwt.auth');
    Route::post('/delete','CategoryController@deleteCategory');//->middleware('jwt.auth');

    //add, get dan delete butuh middleware(harus pakai token)
});

Route::group(['prefix' => 'vendor'], function(){
    Route::get('/','vendorController@getData');//->middleware('jwt.auth');
    Route::post('/add','vendorController@addData');//->middleware('jwt.auth');
    Route::post('/delete','vendorController@deleteData');//->middleware('jwt.auth');

    //add, get dan delete butuh middleware(harus pakai token)
});

Route::post('/login', 'AuthenticationController@login');
Route::post('/register', 'AuthenticationController@register');
