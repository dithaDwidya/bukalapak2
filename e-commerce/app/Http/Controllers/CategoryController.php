<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\category;

class CategoryController extends Controller
{
    function getAllCategory()
        {
            $category = category::get(); //brand adalah model

            return response()->json($category, 200);
        }

    function addCategory(Request $request)
        {
            //DB transaction
            DB::beginTransaction(); //untuk membungkus transaksi, fungsinya adalah jika insert data sukses maka data 
            // akan dicommit dan disave ke database, dan jika gagal maka data akan dirollback

            try
                {
                    // validasi client untuk input
                    $this->validate($request,[
                        'category_name' => 'required', 
                        'parent_id' => 'required'
                    ]);

                    //save ke database
                    $category_name = $request->input('category_name'); //data diambil dari client ke server
                    $brand_name = $request->input('brand_name');
                    $parent_id = $request->input('parent_id');

                    // save ke database menggunakan metode eloquen
                    $newCategory = new category;
                    $newCategory->category_name= $category_name;
                    $newCategory->brand_name = $brand_name;
                    $newCategory->parent_id = $parent_id;
                    $newCategory->save();

                     $catgr = category::get();

                    DB::commit(); //jika insert data sukses maka data akan dicommit dan disave ke database
                    return response()->json($catgr, 200); //saat add user maka akan melakukan add $usrList
                }
            catch (\Exception $e)
                {
                    DB::rollback(); //dan jika gagal maka data akan dirollback
                    return response()->json(["message" => $e->getMessage()], 500); //500->internal server error
                }
        }

    function deleteCategory(Request $request)
        {
            DB::beginTransaction();
            try
                {
                    $this->validate($request,[
                        'category_id' => 'required' //required artinya data tidak boleh kosong
                    ]);

                    $category_id = (integer)$request->input('category_id');
                    $category = category::find($category_id);

                    if(empty($category))
                        {
                            return response()->json(["message" => "User not found"], 404);
                        }

                    $category->delete();

                    $ctgr = category::get();

                    // $data = UserList::where('id','=',$request->input("id"))->delete();
                    DB::commit();
                    return response()->json(["message" => "Success !!"], 200);
                }
            catch (\Exception $e)
                {
                    DB::rollback();
                    return response()->json(["message" => $e->getMessage()], 500);
                }    
        }
}
