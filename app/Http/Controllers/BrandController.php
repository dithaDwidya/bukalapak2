<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\brand;

class BrandController extends Controller
{
    function getAllBrand()
        {
            $brand = brand::get(); //brand adalah model

            return response()->json($brand, 200);
        }

    function addBrand(Request $request)
        {
            //add gambar
            $this->validate($request, [
                'image' => 'required|image:jpeg, png, jpg, gif, svg|max:2048',
            ]);

            //mekanisme upload gambar dari folder public belum berhubungan dengan database
            $name = $request->input('name');

            $image = $request->file('image');
                                //(time,image dapat diganti dengan nama lain)
            $input['imagename'] = time(). '.' .$image->getClientOriginalExtension();
            $destinationPath = public_path('/image'); //untuk bikin sub folder pada public image public_path('/image/blabla')
            $image->move($destinationPath, $input['imagename']);

            return $destinationPath . '/' . $input['imagename'] . ' | Name : ' . $name;

            $brand->logo = 'images/' . $input['imagename'];
            $brand->logo = 'loalhost:8000/images/' . $input['imagename'];
            // $destinationPath . '/' . $input

            //DB transaction
            DB::beginTransaction(); //untuk membungkus transaksi, fungsinya adalah jika insert data sukses maka data 
            //akan dicommit dan disave ke database, dan jika gagal maka data akan dirollback

            try
                {
                   $this->validate($request,[
                        'brand_name' => 'required', //required artinya data tidak boleh kosong
                        'logo' => 'required' // email tidak boleh sama
                    ]);

                    //save ke database
                    $brand_name = $request->input('brand_name'); //data diambil dari client ke server
                    $logo = $request->input('logo');

                    // save ke database menggunakan metode eloquen
                    $newBrand = new brand;
                    $newBrand->brand_name = $brand_name;
                    $newBrand->logo = $logo;
                    $newBrand->save();

                    $brn = brand::get();

                    DB::commit(); //jika insert data sukses maka data akan dicommit dan disave ke database
                    return response()->json($brn, 200); //saat add user maka akan melakukan add $usrList
                }
            catch (\Exception $e)
                {
                    DB::rollback(); //dan jika gagal maka data akan dirollback
                    return response()->json(["message" => $e->getMessage()], 500); //500->internal server error
                }
        }

    function deleteBrand(Request $request)
        {
            DB::beginTransaction();
            try
                {
                    $this->validate($request,[
                        'brand_id' => 'required' //required artinya data tidak boleh kosong
                    ]);

                    $brand_id = (integer)$request->input('brand_id');
                    $brand_id = brand::find($brand_id);

                    if(empty($brand_id))
                        {
                            return response()->json(["message" => "User not found"], 404);
                        }

                    $brand_id->delete();

                    $brand_id = brand::get();

                    // $data = UserList::where('id','=',$request->input("id"))->delete();
                    DB::commit();
                    return response()->json(["message" => "Success !!"], 200);
                }
            catch (\Exception $e)
                {
                    DB::rollback();
                    return response()->json(["message" => $e->getMessage()], 500);
                }    
        }
}
